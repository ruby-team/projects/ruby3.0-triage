Note that sorted_set has been removed from the standard library. It needs to be
packaged separately, and when that's done this package will need an explicit
dependency on it.
